import React from 'react';
import './App.css';
import Engine from './engine';
import {EmptyGame, Game} from "./components";
// import ReactJson from "react-json-view";

interface State {
  game: Game
}

class App extends React.Component<any, State> {

  constructor(props: any) {
    super(props);
    this.state = {
      game: EmptyGame
    }
  }

  componentDidMount(): void {
    Engine.instance().add((game) => {
      this.setState({
        game
      });
      console.log("It's been hit fam");
      console.log(game.pot.maxBet);
      // console.log(game)
    })
  }

  render() {
    return (
        <div className="App">
          {/*<header className="App-header">*/}
          {/*  <p>*/}
          {/*    {this.state.game.gameName}*/}
          {/*  </p>*/}
          {/*  /!*<ReactJson src={this.state.game}/>*!/*/}
          {/*</header>*/}
          <div className="horizontal">
              <div className="vertical">
                  {this.state.game.playerList.map((player, index, playerList) => {
                      return (<div className="child">
                          {player.name + ', ' + player.chips + ' chips'}
                      </div>)
                  })}
              </div>
              <div className="vertical">
                  <div className="child">
                      {this.state.game.pot.totalPot}
                  </div>
                  <div className="horizontal">
                      {this.state.game.boardCards.map((card, index, boardCards) => {
                          return (<div>
                              {card.rawRank + ' of ' + card.rawSuit}
                          </div>)
                      })}
                  </div>
              </div>
              <div className="vertical">
                  {this.state.game.messageHistory.map((message, index, messageHistory) => {
                      return (<div className="child">
                          {message.message}
                      </div>)
                  })}
              </div>
          </div>
        </div>
  );}
}

export default App;
