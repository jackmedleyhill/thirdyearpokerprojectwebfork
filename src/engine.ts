import firebase from "firebase";
import {EmptyGame, Game} from "./components";

class Engine {
    private observers: ((game: Game) => void)[] = [];

    add(observer: (game: Game) => void) {
        this.observers.push(observer);
        observer(this.game)
    }

    game: Game = EmptyGame;

    constructor() {
        const db = firebase.firestore();

        const docRef = db.collection("game-data-collection")
            .doc("Demo");

        const self = this;

        docRef.onSnapshot(function(doc) {
            if (doc.exists) {
                self.game = doc.data() as any as Game;
                self.observers.forEach((a) => {
                    a(self.game)
                });
                console.log("Document Data:", doc.data());
            } else {
                console.log("No such document!");
            }
        })
        //     .catch(function(error) {
        //     console.log("Error getting document:", error);
        // })
    }
}
let engine: Engine | null = null;
export default {
    instance:() => {
        if (engine == null) {
            engine = new Engine();
        }
        return engine;
    }
}