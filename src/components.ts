export interface Card {
    rawRank: string
    rawSuit: string
}

export const EmptyCard: Card = {
    rawRank: "",
    rawSuit: ""
};

export interface Deck {
    deckList: Card[]
}

export const EmptyDeck: Deck = {
    deckList: []

};

export interface Message {
    message: string
    time: string
}

export const EmptyMessage: Message = {
    message: "",
    time: ""
};

export interface Player {
    chips: number
    firstCard: Card
    index: number
    name: string
    rawState: string
    secondCard: Card
}

export const EmptyPlayer: Player = {
    chips: 0,
    firstCard: EmptyCard,
    index: 0,
    name: "",
    rawState: "",
    secondCard: EmptyCard
};

export interface Pot {
    maxBet: number
    numberOfPlayers: number
    playerBets: number[]
    totalPot: number
}

export const EmptyPot: Pot = {
    maxBet: 0,
    numberOfPlayers: 0,
    playerBets: [],
    totalPot: 0
};


export interface Game {
    bigBlindIndex: number
    blindValue: number
    boardCards: Card[]
    currentPlayerIndex: number
    deck: Deck
    gameName: string
    messageHistory: Message[]
    numberOfPlayers: string
    playerList: Player[]
    pot: Pot
    rawGameState: string
    rawHasRaisedState: string
    roundPlayerIndex: number
    smallBlindIndex: number
}

export const EmptyGame: Game = {
    bigBlindIndex: 0,
    blindValue: 0,
    boardCards: [],
    currentPlayerIndex: 0,
    deck: EmptyDeck,
    gameName: "",
    messageHistory: [],
    numberOfPlayers: "",
    playerList: [],
    pot: EmptyPot,
    rawGameState: "",
    rawHasRaisedState: "",
    roundPlayerIndex: 0,
    smallBlindIndex: 0
};

