import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import firebase from "firebase";

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: "AIzaSyB_dR-mRgDxC-lMYE4J_IIaNj4YAt9oxOc",
    authDomain: "third-year-poker-project.firebaseapp.com",
    databaseURL: "https://third-year-poker-project.firebaseio.com",
    projectId: "third-year-poker-project",
    storageBucket: "third-year-poker-project.appspot.com",
    messagingSenderId: "407405870005",
    appId: "1:407405870005:web:34544ee8ed9557bd40b7fc",
    measurementId: "G-RV9DVGDH4H"
});

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
